Cross validation & Feature selection: ===qt bug===
precision	recall	f1	AUC
0.6998616874135546	0.4737827715355805	0.5650474595198214	0.7652496173084997
Selected attribute:
deletion addition total test script config other changed_file_30 total_changed_90 word2vec_f26 word2vec_f33 word2vec_f57 word2vec_f64 word2vec_f93 total_changes undorate label 

Cross validation & Feature selection: ===qt feature===
precision	recall	f1	AUC
0.7299382716049383	0.7310664605873262	0.7305019305019306	0.7773716019776827
Selected attribute:
deletion addition total config other total_changed_90 word2vec_f26 undorate label 

Cross validation & Feature selection: ===qt test===
precision	recall	f1	AUC
0.7060344827586207	0.5504032258064516	0.6185800604229607	0.7892613698705289
Selected attribute:
deletion addition isFeature total source test script config changed_file_30 total_changed_30 total_changed_90 word2vec_f20 word2vec_f22 word2vec_f26 word2vec_f33 word2vec_f74 word2vec_f77 userid undorate label 

Cross validation & Feature selection: ===qt resource===
precision	recall	f1	AUC
0.684931506849315	0.39447731755424065	0.5006257822277848	0.7635549301187388
Selected attribute:
deletion addition isFeature total source test script config changed_file_90 total_changed_30 total_changed_90 word2vec_f5 word2vec_f7 word2vec_f10 word2vec_f12 word2vec_f16 word2vec_f26 word2vec_f29 word2vec_f33 word2vec_f52 word2vec_f54 word2vec_f68 word2vec_f77 word2vec_f79 word2vec_f83 word2vec_f89 word2vec_f93 userid redo_changes undorate label 

Cross validation & Feature selection: ===qt merge===
precision	recall	f1	AUC
0.582089552238806	0.5131578947368421	0.5454545454545455	0.7659107876073161
Selected attribute:
deletion addition total test other changed_file_90 word2vec_f2 word2vec_f7 word2vec_f22 word2vec_f33 word2vec_f34 word2vec_f41 word2vec_f44 word2vec_f57 word2vec_f63 word2vec_f66 word2vec_f73 undorate label 

Cross validation & Feature selection: ===qt deprecated===
precision	recall	f1	AUC
0.684461391801716	0.5077793493635078	0.5830288266341859	0.7665959159881873
Selected attribute:
deletion addition total test script config changed_file_30 total_changed_30 total_changed_90 word2vec_f3 word2vec_f26 word2vec_f68 word2vec_f77 undorate label 

Cross validation & Feature selection: ===qt refactoring===
precision	recall	f1	AUC
0.6993670886075949	0.6443148688046647	0.6707132018209407	0.7589507676806052
Selected attribute:
deletion addition total source test script other total_changed_30 total_changed_90 word2vec_f10 word2vec_f14 word2vec_f17 word2vec_f26 word2vec_f33 word2vec_f50 word2vec_f54 word2vec_f57 word2vec_f64 word2vec_f73 word2vec_f89 word2vec_f96 word2vec_f97 undorate label 

Cross validation & Feature selection: ===qt others===
precision	recall	f1	AUC
0.6533333333333333	0.5132470733210105	0.5748792270531402	0.7351741692961656
Selected attribute:
deletion addition script config other total_changed_30 total_changed_90 word2vec_f16 undorate label 

Cross validation & Feature selection: ===qt multiple===
precision	recall	f1	AUC
0.6996539792387543	0.4926900584795322	0.5782098941950242	0.7801969973612957
Selected attribute:
deletion addition isFeature total source test script config changed_file_90 total_changed_30 total_changed_90 word2vec_f12 word2vec_f20 word2vec_f26 word2vec_f33 word2vec_f68 word2vec_f93 userid undorate label 

Cross validation & Feature selection: ===qt single===
precision	recall	f1	AUC
0.7006139400505598	0.5544441268933981	0.6190172303765156	0.7723565430154268
Selected attribute:
deletion addition isFeature total test script config other changed_file_90 total_changed_30 total_changed_90 word2vec_f20 word2vec_f26 word2vec_f33 word2vec_f57 total_changes undorate label 

Cross validation & Feature selection: ===qt all===
precision	recall	f1	AUC
0.604541506390371	0.5383425414364641	0.5695248115027179	0.7072671421367565
Selected attribute:
deletion addition total source test script config other changed_file_90 total_changed_30 total_changed_90 word2vec_f20 word2vec_f26 word2vec_f33 word2vec_f57 word2vec_f93 undorate label 