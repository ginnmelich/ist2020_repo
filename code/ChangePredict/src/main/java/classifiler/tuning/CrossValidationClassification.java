package classifiler.tuning;

import weka.core.Instances;

import java.util.ArrayList;
import java.util.Random;
import weka.core.converters.ConverterUtils.DataSource;
import weka.attributeSelection.CfsSubsetEval;
import weka.attributeSelection.GreedyStepwise;
import weka.classifiers.Classifier;
import weka.classifiers.Evaluation;
import weka.classifiers.meta.AttributeSelectedClassifier;
import weka.classifiers.trees.J48;
import weka.classifiers.trees.RandomForest;

public class CrossValidationClassification {
	public static void main(String args[]) throws Exception {
		
	    String project = "android";
		
		String bugpath = "../data/"+project+"/arff/bug";
		String featurepath = "../data/"+project+"/arff/feature";
		String testpath = "../data/"+project+"/arff/test";
		String resourcepath = "../data/"+project+"/arff/resource";
		String mergepath = "../data/"+project+"/arff/merge";
		String deprecatpath = "../data/"+project+"/arff/deprecated";
		String refactoringpath = "../data/"+project+"/arff/refactoring";
		String others = "../data/"+project+"/arff/others";
		String multiple = "../data/"+project+"/arff/multiple";
		String single = "../data/"+project+"/arff/single";
		
		ArrayList<String>files = new ArrayList<String>();
		files.add(bugpath);files.add(featurepath);files.add(testpath);files.add(resourcepath);
		files.add(mergepath);files.add(deprecatpath);files.add(refactoringpath);files.add(others);
		files.add(multiple);files.add(single);
		
		for(String str: files) {
			String [] temp = str.split("\\\\");
			System.out.println("==="+project+" "+temp[temp.length-1]+"===");
			
			DataSource source = new DataSource(str+".arff");
			Instances dataset = source.getDataSet();
			dataset.setClassIndex(dataset.numAttributes() - 1);
			dataset.randomize(new java.util.Random(1));
			
			String[] option = new String[2];
			option[0] = "-I";
			option[1] = "1000";
			
			RandomForest randomforest = new RandomForest();
			randomforest.setOptions(option);
			
			Classifier cls = randomforest;
			Evaluation eval = new Evaluation(dataset);
			Random rand = new Random(1); // using seed = 1
			int folds = 10;
			eval.crossValidateModel(cls, dataset, folds, rand);
			//System.out.println(eval.toSummaryString());
			
			System.out.println("precision	recall	f1	AUC");
			System.out.println(eval.precision(1)+"	"+eval.recall(1)+"	"+eval.fMeasure(1)+"	"+eval.areaUnderROC(1));
		}
	}
}
